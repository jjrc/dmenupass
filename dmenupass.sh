#!/bin/sh
# dmenupass - write passwords through dmenu.
# Source code.

. /usr/local/etc/dmenupass/config.sh

dmenucmd -P -p "$1" <&- || exit 1
