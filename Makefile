# dmenupass - write passwords through dmenu. 
# Makefile.

NAME=dmenupass
PREFIX=/usr/local

options:
	@echo no compilation is needed:
	@echo - run \"make install\" to install $(NAME)
	@echo - run \"make uninstall\" to uninstall $(NAME)

install: config.sh $(NAME).sh
	mkdir -p $(PREFIX)/bin
	mkdir -p $(PREFIX)/etc/$(NAME)
	cp -f config.sh $(PREFIX)/etc/$(NAME)
	cp -f $(NAME).sh $(PREFIX)/bin/$(NAME)
	chmod 644 $(PREFIX)/etc/$(NAME)/config.sh
	chmod 755 $(PREFIX)/bin/$(NAME)

uninstall:
	rm -f $(PREFIX)/bin/$(NAME)
	rm -rf $(PREFIX)/etc/$(NAME)

.PHONY: options install uninstall
