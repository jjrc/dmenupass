# dmenupass - write passwords through dmenu.
# Configuration file.  

# appearance
DMENUFONT="monospace:size=11"
COL_GRAY1="#222222"
COL_GRAY2="#bbbbbb"
COL_GRAY3="#eeeeee"
COL_GOLD="#806000"
DMENUMON="0"

# command
alias dmenucmd="dmenu -m $DMENUMON -fn \"$DMENUFONT\" -nb \"$COL_GRAY1\" -nf \"$COL_GRAY2\" -sb \"$COL_GOLD\" -sf \"$COL_GRAY3\""
